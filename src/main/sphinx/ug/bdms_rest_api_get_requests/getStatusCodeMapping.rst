###################
Status Code Mapping
###################

.. toctree::
   :maxdepth: 2



.. code-block:: console

    GET /v1/status-code-mappings/{dataModelName}/{dataViewName}


**Description**

Returns status code mappings.


GET Status Code Mapping
=======================

**Response (JSON)**

.. code-block:: console

    Response (JSON)
    [
    {
    "statusCode": 0,
    "statusDescription": "Not available"
    },
    {
    "statusCode": 1,
    "statusDescription": "Normal"
    },
    {
    "statusCode": 2,
    "statusDescription": "Percentage failure"
    },
    {
    "statusCode": 3,
    "statusDescription": "Manually corrected"
    },
    {
    "statusCode": 4,
    "statusDescription": "Manually validated"
    },
    {
    "statusCode": 5,
    "statusDescription": "Estimated"
    }
    ]










