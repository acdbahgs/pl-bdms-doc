#########################
Attribute Classifications
#########################

.. toctree::
   :maxdepth: 2



.. code-block:: console

    GET {BASE_URL}/v1/attribute-classifications



**Description**

Returns domain model attribute  classifications.


GET attribute  Classifications
==============================

**Response**

.. code-block:: console

    [
    {
        "name": "Time-Series",
        "description": "TimeSeries attribute"
    },
    {
        "name": "Reference Data",
        "description": "Static attribute"
    }
    ]










