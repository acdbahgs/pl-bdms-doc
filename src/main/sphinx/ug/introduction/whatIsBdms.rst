##################
What is |project|?
##################

.. toctree::
   :maxdepth: 2


Background
==========
AC Plus has an extensive proprietary data model template library designed from the main asset classes (e.g. Equities, Fixed Income, etc.) to represent financial instruments sharing similar characteristics. They represent the ideal foundation for building an asset class based data dictionary for the business as they already capture the logical data elements and the relationship between them, such as (1) how the elements are used together (2) their origins, and (3) their descriptions.

Problem Statement
=================
The proprietary nature of the toolkit makes it a challenge for non-AC developers to extract metadata attribute information from the AC data models to designate key attributes in business terms, to extend and categorize the instruments based on asset classes. A common approach is to develop a report from the business requirements that dictate the list of AC data model elements to report on, how they should be called and in which format they should be delivered (e.g. csv file, xml, etc.).

Solution
========
There needs to be a standard mechanism that leverages the underlying AC logical data models as the foundational meta data such that the business is able to easily categorize/organize key data elements around names and relationships that clearly define what the data means.

Business Domain Model Service
=============================
Business Domain Model Service, BDMS for short, provides the standard, non-proprietary approach to building a "Business Domain" Model with a list of specific terms with meanings familiar to the business community and to categorize by asset classes. BDMS uses the underlying AC logical data models as the starting point for categorizing the data in terms views of Data, Class and Attribute.

Initial Set up
==============
The goal is to first define an abstraction layer consisting of key data elements of interest categorized in terms of data, class and attribute views the business community is familiar with.

.. figure:: /img/bdms_initial_setup_01.png
   :alt: BDMS Initial Setup
   :align: center

   BDMS Initial Setup

BDMS Data Model files
---------------------

Upcoming releases of Interface modules in the near future will include the following enhancements to their data model specification files (i.e. .tsv sheets) and the tools to support BDMS.

For example, in case of the Bloomberg interface, 3 additional columns will be added to bb_sap.tsv (Static Attribute Properties ) and bb_dap.tsv (Datafile Attribute Properties) files

* Attribute.Description
* Attribute.IsDerived
* Attribute.Classification

2. Additional BDMS tsv files will be added:

* bb_bdm_cls
* bb_bdm_att
* bb_bdm_cfi

3. A utility (installdm??) will be available to generate the BDMS JSON file

4. TDB

Once the Business Domain Model is installed, the data can be accessed and manipulated through a standard, non-proprietary RESTful API layer.


Simplification
==============
BDMS introduces the ability to alias AC logical data model elements as Views. The following types of Views exist:

* Data Views
* Class Views
* Attribute Views

Views hide the complexity of the logical data model structures stored in the database to ease the user interaction with the actual data through meaningful descriptions of what the data represents. BDMS views are configurable to fit the requirements from the business in terms of descriptive view names.

.. figure:: /img/bdms_Views_02.png
   :alt: BDMS Views: Data, Class, Attribute
   :align: center

   BDMS Views: Data, Class, Attribute


Once the BDM is setup, REST API is used to interact with it.


