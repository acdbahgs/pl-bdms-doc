##############
Data View Name
##############

.. toctree::
   :maxdepth: 2



.. code-block:: console

    GET /v1/class-view-name/{dataModelName}/{dataViewName}/{objectId}


**Description**

Returns class view name.


GET Data View Name
==================

**Response (string)**

.. code-block:: console

    Instrument Stock










