####################
Normalized Data View
####################

.. toctree::
   :maxdepth: 2

**Request GET Data View**

.. code-block:: console

    GET {BASE_URL}/v1/dataview/{Data Model Name}
    where {Data Model Name} is the Data Model Name

**Description**

Get the Data View name associated to {Data Model Name}


Example: GET Normalized Data View associated to the "NormalizedModel"
=======================================================

To get the Data View associated to the Normalized Data Model named, "NormalizedModel"

**Request GET**

.. code-block:: console

    {BASE_URL}/v1/dataview/NormalizedModel

**Description**

Get the Data View name associated to "BloombergModel"

**Response**

.. code-block:: console

    Response-full (JSON): GET Data View associated to NormalizedModel
    [
        "Normalized View"
    ]
