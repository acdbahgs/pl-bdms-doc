##################
Data Model Details
##################

.. toctree::
   :maxdepth: 2


Request GET Data Model Details
==============================

.. code-block:: console

    {BASE_URL}/v1/datamodel
    where {BASE_URL} is the server and port number (e.g. http://ops360server:6969 )

Description
===========

The query returns one list that consists of all the data models.
Each data model contains a complete listing of associated classes with a complete listing of attributes for each class.
The response contains all the details associated to each Data Model, namely Class and Attribute details.


Response-structure (JSON): BDMS Data Model Details
===================================================

The response file below has been simplified to only show the data model information with minimal class and attribute details.
There are details on three data models in the response JSON file below:

* DemoModel (Consolidated Model (Sample))
* BloombergModel (Bloomberg Model)
* NormalizedModel (Normalized Model (Sample))


.. code-block:: console

    [{
            "name": "DemoModel",
            "description": "Consolidated Model (Sample)",
            "classes": [{
                    "name": "ISSUER",
                    "description": "Issuer",
                    "attributes": [{
                            "name": "C0+IS100",
                            "description": "Child entity ADO",
                            "type": "ISSUER_ADO",
                            "writable": true,
                            "classification": "Hierarchy"
                        },
     				...,
     				{
                            "name": "C0+IS015",
                            "description": "Issuer instrument",
                            "type": "ISSUER_INSTRUMENT",
                            "writable": true,
                            "classification": "Identifiers"
                        }

                    ]
                }, {
                    "name": "INSTRUMENT_STK",
                    "description": "Instrument ISS_STK",
                    "attributes": [{},...,{} ]
                }, {
                    "name": "INSTRUMENT_CDS",
                    "description": "Instrument ISS_CDS",
                    "attributes": [{},...,{} ]
                },
     		...,
     		{
                    "name": "RATING",
                    "description": "MARKIT RATING",
                    "attributes": [{},...,{} ]
                }
            ]
        }, {
            "name": "BloombergModel",
            "description": "Bloomberg Model",
            "classes": [
     			{"name": "Govt","description": "Government Bond","attributes": [{},...,{}] },
     			{"name": "Equity","description": "Equity","attributes": [{}, {},...,{}]},
     			{"name": "Corp","description": "Corporate Bond","attributes": [{},...,{}]},
     			...,
     			{"name": "Muni","description": "Municipal Bond","attributes": [{},...,{}]}
            ]
        }, {
            "name": "NormalizedModel",
            "description": "Normalized Model (Sample)",
            "classes": [
     			{"name": "Entity","description": ...,"attributes": [{},...,{}]},
     			...
                    {"name": "Futures","description": "Futures",...,"attributes": [{},...,{}]}
                }
            ]
        }
    ]


Response (JSON): BDMS Data Model Details
=============================================

Here is more content of the response file: all attribute and class information of a data model is nested inside the Data Model

.. code-block:: console

    [
    {
        "name": "DemoModel",
        "description": "Consolidated Model (Sample)",
        "classes": [{
                "name": "ISSUER",
                "description": "Issuer",
                "attributes": [{
                        "name": "C0+IS100",
                        "description": "Child entity ADO",
                        "type": "ISSUER_ADO",
                        "writable": true,
                        "classification": "Hierarchy"
                    }, {
                        "name": "C0_EOD.NL.NYSE.HIGH",
                        "description": "High Price",
                        "type": "TIMESERIES_COMPOUND_PRICE",
                        "writable": true,
                        "classification": "Pricing"
                    },
					{},...{},
					{
                        "name": "C0+IS015",
                        "description": "Issuer instrument",
                        "type": "ISSUER_INSTRUMENT",
                        "writable": true,
                        "classification": "Identifiers"
                    }
                ]
            }, {
                "name": "INSTRUMENT_STK",
                "description": "Instrument ISS_STK",
                "attributes": [{
                        "name": "C0_I0045",
                        "description": "DV Issuer ID",
                        "type": "String",
                        "writable": true,
                        "classification": "Identifiers"
                    }, {
                        "name": "C0_EOD.MID",
                        "description": "Mid Price",
                        "type": "TIMESERIES_PRICE",
                        "writable": true,
                        "classification": "Pricing"
                    },
					{},...{},
					{
                        "name": "C0_I0058",
                        "description": "Payment currency",
                        "type": "CURRENCY",
                        "writable": true,
                        "classification": "Income"
                    }
                ]
            }, {
                "name": "INSTRUMENT_CDS",
                "description": "Instrument ISS_CDS",
                "attributes": [{
                        "name": "C0_I0372",
                        "description": "Long Description",
                        "type": "String",
                        "writable": true,
                        "classification": "Description"
                    },{},...{},
					{
                        "name": "C0_I0131",
                        "description": "Security type  tertiary",
                        "type": "SECURITY_TYPE_CODE_TERTIARY_(N01.4)",
                        "writable": true,
                        "classification": "Description"
                    }
                ]
            }, {
                "name": "INSTRUMENT_BND",
                "description": "Instrument ISS_BND",
                "attributes": [ {},...{} ]
                ]
            }, {
                "name": "LISTING_STK",
                "description": "Listing ISS_STK",
                "attributes": [{
                        "name": "C0+I0095",
                        "description": "Issuer",
                        "type": "ISSUER_ADO",
                        "writable": true,
                        "classification": "Identifiers"
                    }, {},...{},
					{
                        "name": "C0_I0007",
                        "description": "Trade country",
                        "type": "TRADE_COUNTRY",
                        "writable": true,
                        "classification": "Codification"
                    }
                ]
            }, {
                "name": "LISTING_CDS",
                "description": "Listing ISS_CDS",
                "attributes": [{
                        "name": "C0_I0247",
                        "description": "Currency",
                        "type": "CURRENCY",
                        "writable": true,
                        "classification": "Codification"
                    },
					{},...{},
					{
                        "name": "C0_EOD.HIGH",
                        "description": "High Price",
                        "type": "TIMESERIES_PRICE",
                        "writable": true,
                        "classification": "Pricing"
                    }
                ]
            }, {
                "name": "LISTING_BND",
                "description": "Listing ISS_BND",
                "attributes": [{
                        "name": "C0_I0074",
                        "description": "Trading unit increments above minimum",
                        "type": "Float",
                        "writable": true,
                        "classification": "Codification"
                    }, {
                        "name": "C0_EOD.OPEN",
                        "description": "Open Price",
                        "type": "TIMESERIES_PRICE",
                        "writable": true,
                        "classification": "Pricing"
                    },
					{},...{},
					{
                        "name": "C0+I0095",
                        "description": "Issuer",
                        "type": "ISSUER_ADO",
                        "writable": true,
                        "classification": "Identifiers"
                    }
                ]
            }, {
                "name": "RATING",
                "description": "MARKIT RATING",
                "attributes": [{
                        "name": "MARKIT.C0.MI+0226",
                        "description": "Sector changes",
                        "type": "TIMESERIES_SECTOR_CHANGES",
                        "writable": true,
                        "classification": "CurvesGroup"
                    }, {
                        "name": "MARKIT.C0.MI+0225",
                        "description": "Sector level",
                        "type": "TIMESERIES_SECTOR_LEVEL",
                        "writable": true,
                        "classification": "CurvesGroup"
                    }
                ]
            }
        ]
    }, {
        "name": "BloombergModel",
        "description": "Bloomberg Model",
        "classes": [{
                "name": "Govt",
                "description": "Government Bond",
                "attributes": [{
                        "name": "BB_DS219",
                        "description": "Announce Date",
                        "type": "Date",
                        "writable": true,
                        "classification": "GeneralInformation"
                    },
					{},...{},
					{
                        "name": "BB_FLW_LASTUPDATE",
                        "description": "Date of last update",
                        "type": "DateTime",
                        "writable": true,
                        "classification": "GeneralInformation"
                    }
                ]
            }, {
                "name": "Equity",
                "description": "Equity",
                "attributes": [{
                        "name": "BB_ID035",
                        "description": "Bloomberg Company Identifier",
                        "type": "String",
                        "writable": true,
                        "classification": "GeneralInformation"
                    }, {
                        "name": "BLOOMBERG_DATA.BB_PR007",
                        "description": "High Price",
                        "type": "TIMESERIES_PRICE",
                        "writable": true,
                        "classification": "Pricing"
                    },
					{},...{},
					{
                        "name": "BB_ID135",
                        "description": "Financial Instrument Global Identifier",
                        "type": "String",
                        "writable": true,
                        "classification": "Identifiers"
                    }
                ]
            }, {
                "name": "Corp",
                "description": "Corporate Bond",
                "attributes": [{
                        "name": "BB_ID229",
                        "description": "Bloomberg Global Identifier Company Name",
                        "type": "String",
                        "writable": true,
                        "classification": "Identifiers"
                    },{},...{},
					{
                        "name": "BB_DS173",
                        "description": "First Settle Date",
                        "type": "Date",
                        "writable": true,
                        "classification": "GeneralInformation"
                    }
                ]
            }, {
                "name": "Curncy",
                "description": "Currency",
                "attributes": [{
                        "name": "BB_DS001",
                        "description": "Ticker",
                        "type": "String",
                        "writable": true,
                        "classification": "Identifiers"
                    },
					{},...{}
                ]
            }, {
                "name": "Index",
                "description": "Index",
                "attributes": [{
                        "name": "BB_DS001",
                        "description": "Ticker",
                        "type": "String",
                        "writable": true,
                        "classification": "Identifiers"
                    },
					{},...{}
                ]
            }, {
                "name": "Muni",
                "description": "Municipal Bond",
                "attributes": [{
                        "name": "BB_DS001",
                        "description": "Ticker",
                        "type": "String",
                        "writable": true,
                        "classification": "Identifiers"
                    },
					{},...{}
                ]
            }
        ]
    }, {
        "name": "NormalizedModel",
        "description": "Normalized Model (Sample)",
        "classes": [{
                "name": "Entity",
                "description": "Entity",
                "attributes": [{
                        "name": "N0_IS006",
                        "description": "Issuer entity status indicator",
                        "type": "ISSUER_STATUS_INDICATOR",
                        "writable": true,
                        "classification": "Description"
                    }, {
                        "name": "N0_SRC25",
                        "description": "Issuer entity Name",
                        "type": "String",
                        "writable": true,
                        "classification": "ObjectInformation"
                    },
					{},...{},
					{
                        "name": "N0_IS060",
                        "description": "Issuer Moodys ratings",
                        "type": "MOODY'S_LT_LC_ISSUER_RATING",
                        "writable": true,
                        "classification": "Ratings"
                    }
                ]
            }, {
                "name": "Equity",
                "description": "Equity",
                "attributes": [{
                        "name": "N0_I0288",
                        "description": "Active status",
                        "type": "INSTRUMENT_STATUS",
                        "writable": true,
                        "classification": "Description"
                    },
					{},...{},
					{
                        "name": "N0_I0110",
                        "description": "First coupon date",
                        "type": "Date",
                        "writable": true,
                        "classification": "Computation"
                    }
                ]
            }, {
                "name": "Debt",
                "description": "Debt",
                "attributes": [{
                        "name": "N0_I0035",
                        "description": "ID ISIN",
                        "type": "String",
                        "writable": true,
                        "classification": "Identifiers"
                    },
					{},...{},
					{
                        "name": "N0_I0058",
                        "description": "Payment currency",
                        "type": "CURRENCY",
                        "writable": true,
                        "classification": "Income"
                    }
                ]
            }, {
                "name": "Entitlements",
                "description": "Entitlements",
                "attributes": [{
                        "name": "N0_I0288",
                        "description": "Active status",
                        "type": "INSTRUMENT_STATUS",
                        "writable": true,
                        "classification": "Description"
                    },
					{},...{},
					{
                        "name": "N0_I0131",
                        "description": "Security type  tertiary",
                        "type": "SECURITY_TYPE_CODE_TERTIARY_(N01.4)",
                        "writable": true,
                        "classification": "Description"
                    }
                ]
            }, {
                "name": "Futures",
                "description": "Futures",
                "attributes": [{
                        "name": "N0_I0131",
                        "description": "Security type  tertiary",
                        "type": "SECURITY_TYPE_CODE_TERTIARY_(N01.4)",
                        "writable": true,
                        "classification": "Description"
                    },
					{},...{},
					{
                        "name": "N0_I0129",
                        "description": "Security type secondary",
                        "type": "SECURITY_TYPE_CODE_SECONDARY_(N01.4)",
                        "writable": true,
                        "classification": "Description"
                    }
                ]
            }, {
                "name": "Options",
                "description": "Options",
                "attributes": [{
                        "name": "N0_I0020",
                        "description": "Security type primary",
                        "type": "SECURITY_TYPE_CODE_PRIMARY",
                        "writable": true,
                        "classification": "Description"
                    },
					{},...{},
					{
                        "name": "N0_I0131",
                        "description": "Security type  tertiary",
                        "type": "SECURITY_TYPE_CODE_TERTIARY_(N01.4)",
                        "writable": true,
                        "classification": "Description"
                    }
                ]
            }
        ]
    }
    ]
