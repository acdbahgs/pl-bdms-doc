#############################################
Find Consolidated Listing Stock Class Objects
#############################################

.. toctree::
   :maxdepth: 2


.. figure:: /img/bdms_C0_Listing_stock_objects_01.png
   :alt: BDMS Consolidated Listing Stock Class Objects
   :align: center

   BDMS Consolidated Listing Stock Class Objects



GET Consolidated Listing Stock Class Objects - simple find
==========================================================

Example

.. code-block:: console

    GET Consolidated Listing Stock Class Objects (simple-find)

    {BASE_URL}/v1/data/simple-find?DemoModel&dataViewName=Consolidated View&classViewName=Listing Stock

.. csv-table:: Parameters
   :header: "Query Parameter-Key","Query Parameter-Value (Example)" ,"Description"

    dataModelName,DemoModel,Name of the underlying Consolidation Data Model
    dataViewName,Consolidated View,Name of the Consolidation View
    classViewName,Listing Stock,Name of the Consolidation Class View
    pageSize,pageSize=100
    pageNumber,pageNumber=0
    sortAttributeName,sortAttributeName=Active status
    ascending,ascending=true

Additionally, the following parameters can be used:

* whereClause

Example

.. code-block:: console

    [
    {"attributeViewName": "Entity type", "value": "INS_BND", "operator": "EQUAL"},
    {"attributeViewName": "Date of issue", "values": ["19800518","20190704"], "operator": "BETWEEN"}
    ]

* dimensionWhereClause

Example

.. code-block:: console
    {

    "included": [
    {"dimension":"Region","point": "UK"},
    {"dimension":"Region","point": "EU"}
    ],
    "excluded":[
    {"dimension":"Industry","point": "Fintech"}
    ]
    }


**Description**

GET Consolidated Listing Stock Class View Objects. Allows searching of data using instrument properties.
A list of object IDs is returned.

**Response**


.. code-block:: console

    Response-full (JSON): simple-find GET Consolidated Listing Stock objectIDs
    [
    "C0.LST.207",
    "C0.LST.00-10065",
    "C0.LST.00-31405",
    "C0.LST.800",
    "C0.LST.204",
    "C0.LST.00-10072",
    "C0.LST.660",
    "C0.LST.201",
    "C0.LST.00-10052",
    "C0.LST.00-31400",
    "C0.LST.00-31443",
    "C0.LST.00-31436",
    "C0.LST.00-10044"
    ]

Fetch Attribute Values (STATIC & TIMESERIES) of Consolidated Listing Stock Class Object
=======================================================================================

.. figure:: /img/bdms_fetch_STATIC_TS_attrvals_C0_Listing_stock_objects_01.png

   :alt: Get STATIC & TIMESERIES attribute values of Consolidated Listing Stock Class Object
   :align: center

   Get STATIC & TIMESERIES attribute values of Consolidated Listing Stock Class Object

GET All Static Attributes of all Consolidated Listing Stock Class Objects
==========================================================================

Example

**Request**

.. code-block:: console

    {BASE_URL}/v1/data/DemoModel/Consolidated View/Listing_Stock?objectIDs={C0_cls_ListingStock}&attributeNames={SA_attrNms_C0_cls_ListingStock}

.. csv-table:: Parameters
   :header: "Query Parameter-Key","Query Parameter-Value (Example)" ,"Description"

    objectIDs,{C0_cls_ListingStock} ["C0.LST.207"],C0 Listing Stock class objectIDs
    attributeNames,{SA_attrNms_C0_cls_ListingStock} ["Currency"],C0 Static Attrs of Listing Stock class obejctIDs

**Response**

.. code-block:: console

    [
    {
        "id": "C0.LST.207",
        "attributeValues": [
            {
                "name": "Currency",
                "values": [
                    {
                        "status": 140,
                        "value": "GBP",
                        "timestamp": "2019-11-19T09:08:06.000+00:00"
                    }
                ]
            }
        ]
    }
    ]

GET All Timeseries Attribute Values of all Consolidated Listing Stock class objects
===================================================================================

**Request**

.. code-block:: console

    {BASE_URL}/v1/data/DemoModel/Consolidated View/Listing_Stock?objectIDs={objIDs_C0_clsVw_ListingStock}&attributeNames={TS_attrNms_C0_clsVw_LST_STK}

.. csv-table:: Parameters
   :header: "Query Parameter-Key","Query Parameter-Value (Example)" ,"Description"

    objectIDs,{objIDs_C0_clsVw_ListingStock} ["C0.LST.207"],C0 Listing Stock class objectIDs
    attributeNames,{TS_attrNms_C0_clsVw_LST_STK}} ["Open Price"],C0 Time-series Attrs of Listing Stock obejcItDs


**Response**

.. code-block:: console

    [
    {
        "id": "C0.LST.207",
        "attributeValues": [
            {
                "name": "Volume",
                "values": [
                    {
                        "status": 3,
                        "value": 200,
                        "timestamp": "2019-08-31T09:20:37.000+00:00"
                    }
                ]
            },
            {
                "name": "Open Price",
                "values": [
                    {
                        "status": 1,
                        "value": null,
                        "timestamp": "2019-08-31T09:20:37.000+00:00"
                    }
                ]
            }
        ]
    }
    ]

























