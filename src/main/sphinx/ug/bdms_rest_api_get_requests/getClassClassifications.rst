#####################
Class Classifications
#####################

.. toctree::
   :maxdepth: 2

.. code-block:: console

    GET {BASE_URL}/v1/class-classifications

    where
    {Data Model Name} is the name of the Data Model
    {Data View Name} is the view name of the Data Model


**Description**

Returns domain model class classifications.


GET Class Classifications
=========================

**Response (JSON) **

.. code-block:: console

    [
    {
        "name": "ISSUER",
        "description": "Issuer"
    },
    {
        "name": "INSTRUMENT_STK",
        "description": "Instrument Stock"
    },
    {},
    {}
    ]










