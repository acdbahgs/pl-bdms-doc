######################
Update Attribute Value
######################

.. toctree::
   :maxdepth: 2


.. code-block:: console

    POST /v1/data/{dataModelName}/{dataViewName}/{classViewName}/{objectId}

    where
    {Data Model Name} is the name of the Data Model
    {Data View Name} is the view name of the Data Model
    {objectId) is


**Description**

Updates the specified attribute value.

.. csv-table:: Post Parameters
   :header: "Post Parameter","Description"

    name,Attribute view name
    values,Attribute values
    comments,Optional parameter

**Example: Post Parameters**

.. code-block:: console

    {
    "name": "Long description",
    "value":
    {
    "value": "HSBC GBP ORD SHS AT XPAR",
    "status": 1,
    "timestamp": "20190823T092037-04:00"
    },
    "comment": "Manually corrected"
    }


**Error messages**

400 - invalid request parameters 403 - if the attribute is not writable.








