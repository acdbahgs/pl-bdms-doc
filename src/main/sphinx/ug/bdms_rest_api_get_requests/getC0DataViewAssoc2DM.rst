#####################
Consolidate Data View
#####################

.. toctree::
   :maxdepth: 2

From our previous example of BDMS Data Model Details, the top level name fields refer to the names of the underlying Data Models we need to use in the GET request to get the Data View information:

* name: *DemoModel*
* name: *BloombergModel*
* name: *NormalizedModel*


**Request GET Data View**

.. code-block:: console

    GET {BASE_URL}/v1/dataview/{Data Model Name}
    where {Data Model Name} is the Data Model Name

**Description**

Get the Data View name associated to {Data Model Name}


Example: GET Consolidated Data View associated to the "DemoModel"
=================================================================

To get the Data View associated to the Consolidated Data Model named, "DemoModel"

**Request GET Consolidated Data View**

.. code-block:: console

    {BASE_URL}/v1/dataview/DemoModel

**Description**

Get the Data View name associated to "DemoModel"

**Response**

.. code-block:: console

    Response-full (JSON): GET Data View associated to DemoModel
    [
        "Consolidated View"
    ]








