################################
Update Multiple Attribute Values
################################

.. toctree::
   :maxdepth: 2



.. code-block:: console

    POST /v1/data/{dataViewName}/values


**Description**

Updates specified attribute values.

.. csv-table:: Post Parameters
   :header: "Post Parameter","Description"

    values,Attribute values

**Example: Post Parameters**

.. code-block:: console

    {
    "attributeValues": [
    {
      "classViewName": "Issuer",
      "objectId": "C0.IS.13",
      "attributeName": "Long description",
      "value": {
        "value": "HSBC GBP ORD SHS AT XPAR",
        "status": 1,
        "timestamp": "20190823T092037-04:00"
    }
    },
    {
      "classViewName": "Equity",
      "objectId": "C0.EQ.67",
      "attributeName": "Long description",
      "value": {
        "value": "HSBC GBP AD GEAS",
        "status": 1,
        "timestamp": "20190823T092037-04:00"
    }
    }
    ],
    "comment": "Manually corrected"
    }

**Error messages**

400 - invalid request parameters 403 - if the attribute is not writable.

.. code-block:: console

    {
    "errors":[
        {
            "objectId":"C0.EQ.67",
            "error":"attribute is read-only"
        },
        {
            "objectId":"C0.EQ.15",
            "error":"value invalid"

        },
    ]
    }











