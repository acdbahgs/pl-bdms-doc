####################
Export Data to Excel
####################

.. toctree::
   :maxdepth: 2

GET /v1/data/excel - Exports data to excel
==========================================

**Request**

.. code-block:: console

    GET /v1/data/excel

**Description**

Allows exporting search results to to an excel file.
pageSize and pageNumber are optional attributes. If they are not provided then all pages are exported.


.. csv-table:: Parameters
   :header: "Query Parameter-Key","Query Parameter-Value (Example)" ,"Description"

    dataModelName,DemoModel,Name of the underlying Consolidation Data Model
    dataViewName,Consolidated View,Name of the Consolidation View
    classViewName,Listing Stock,Name of the Consolidation Class View
    pageSize,pageSize=100
    pageNumber,pageNumber=0
    sortAttributeName,sortAttributeName=Active status
    ascending,ascending=true

Additionally, the following parameters can be used:

* attributeNames: names of the AttributeViews to export

Example

.. code-block:: console

    attributeNames=["Long description","Issuer ID","Entity type","Currency"]



* whereClause

Example

.. code-block:: console

    [
    {"attributeViewName": "Entity type", "value": "INS_BND", "operator": "EQUAL"},
    {"attributeViewName": "Date of issue", "values": ["19800518","20190704"], "operator": "BETWEEN"}
    ]

**Response**

An excel file containing values for attributes specified in attributeNames for ADOs found using provided parameters.





















