################
Get Public Types
################

.. toctree::
   :maxdepth: 2


Request GET Public Tyoes
========================

.. code-block:: console

    {BASE_URL}/v1/publictypes
    where {BASE_URL} is the server and port number (e.g. http://ops360server:6969 )

Description
===========

Returns the PublicType information


Response (JSON): BDMS Public Types Detail
==========================================

Response (JSON)

.. code-block:: console

    {
    "PublicTypes": {
    "GICS INDUSTRY GROUP": {
      "Type": "Enum",
      "ValueType": "String",
      "Values": [
        {
          "Value": "1010",
          "Description": "ENERGY"
        },
        {
          "Value": "1510",
          "Description": "MATERIALS"
        },
        {
          "Value": "2010",
          "Description": "CAPITAL GOODS"
        }
      ]
    },
    "ISSUER_ADO": {
      "Type": "ClassReference",
      "DataModel": "DemoModel",
      "ClassName": "ISSUER"
    },
    "OTC/EXCHANGE_TRADED_INDICATOR_(FROZEN)": {
      "Type": "Enum",
      "ValueType": "String",
      "Values": [
        {
          "Value": "7",
          "Description": "NOT APPLICABLE"
        },
        {
          "Value": "9",
          "Description": "UNKNOWN"
        },
        {
          "Value": "A",
          "Description": "OTC"
        },
        {
          "Value": "B",
          "Description": "EXCHANGE TRADED"
        }
      ]
    },
    "Time": {
      "Type": "Time"
    },
    "String": {
      "Type": "String"
    }
    }
    }

