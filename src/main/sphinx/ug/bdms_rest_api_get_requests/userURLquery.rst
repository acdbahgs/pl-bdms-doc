#################################################
User URL query (search query url sharing feature)
#################################################

.. toctree::
   :maxdepth: 2

.. code-block:: console

    /browse/ac/search?dataModelName=DemoModel&dataViewName=Consolidated+View&classViewName=ISSUER&Issuer+industry+group=EQUAL%2CTRANSPORTATION&Security+type+primary=IN%2CFUTURES%2CEQUITY%2CDEBT&dimensionWhereClause=some_structure_of_it&anykey=anyvalue


**Description**

Allows share search state and use searching of data using instrument properties. This url is parsed via RFC1738 https://www.ietf.org/rfc/rfc1738.txt

**Explanation**

The main idea behind the query format was to achieve simplicity, laconicism and less extra characters. So, the structure was flattened as possible. That's the reason why the query does not have parenthesis in the `whereClause` collection.
The parser extracts three (two in future when only dataViewName is required) predefined keys that define dataView and classView. The rest is attributes. The value of each attribute is an array (comma-separated). The first item of this array is intended for an operator, the rest is values (could be 1 or more). `pageSize`, `pageNumber`, and other key-value pairs could be added to this chain. All white-listed keys easy to extract, where the rest keys are for viewnames of attributes

Parsed top level `&` and `=` separated



.. csv-table:: Post Parameters
   :header: "Key","Value"

    dataModelName,DemoModel
    dataViewName,Consolidated+View
    classViewName,ISSUER
    Issuer+industry+group,EQUAL%2CTRANSPORTATION
    Security+type+primary,IN%2CFUTURES%2CEQUITY%2CDEBT

equal sign (=) separated string


.. csv-table:: Parsed comma-separated array of an operator and attribute values
    :header: key operator,value #1,value #2,value #3,...value #N

    Issuer+industry+group,EQUAL,TRANSPORTATION
    Security+type+primary,IN,FUTURES,EQUITY,DEBT

To parse such string, the well known and very popular package qs is used. It has 27 million downloads per week.
The package has the following options:
{ format: 'RFC1738', skipNulls: true, comma: true }





