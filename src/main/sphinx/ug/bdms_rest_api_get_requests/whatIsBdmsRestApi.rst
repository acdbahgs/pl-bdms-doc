################
BDMS RESTful API
################

.. toctree::
   :maxdepth: 2


BDMS REST API runs on the web over HTTP to give users access to financial instrument data as web resources making it a RESTful API. Below is an example of a WebUI page that illustrates how the BDMS REST API requests might be used.

On the left side of the screen,

1. Data Views pane (on the left side) displays the data aggregation views that are relevant to the business users

2. Class Views pane displays the list of instrument categorization for the selected Data View (e.g. Consolidated View)

3. Attribute Views pane displays "Key" attributes of interest. This list would consists of key reference and timeseries attributes that are a subset of the data attribute universe defined in the AC Plus environment The upper right pane shows all the security objects in the AC Plus system (i.e. ADOs) that belong to the selected Data and Class view. The bottom right pane shows the key attribute values of the selected security object.


.. figure:: /img/bdms_ui_example_01.png
   :alt: Sample UI exposing BDMS views fetched via Rest API
   :align: center

   Sample UI exposing BDMS views fetched via Rest API

.. figure:: /img/bdms_dataviews_01.png
   :alt: BDMS Data Views
   :align: center

   BDMS Data Views




