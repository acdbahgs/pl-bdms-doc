#############
BDMS Rest API
#############

.. toctree::
   :maxdepth: 2

   whatIsBdmsRestApi.rst
   getAllDataModelDetails.rst
   getC0DataViewAssoc2DM.rst
   getVendorSrcDataViewAssoc2BBDM.rst
   getN0DataViewAssoc2DM.rst
   getClassClassifications.rst
   getAttributeClassifications.rst
   getPublicTypes.rst
   getStatusCodeMapping.rst
   updateAttributeValue.rst
   updateMultiAttributeValues.rst
   userURLquery.rst
   exportDataToExcel.rst
   findC0ListingStockClassObjects.rst


