######################
Consolidate Class View
######################

.. toctree::
   :maxdepth: 2


.. figure:: /img/bdms_C0_classviews_01.png
   :alt: BDMS Consolidated Class Views
   :align: center

   BDMS Consolidated Class Views


"viewname" fields are used as resources or as parameter values to BDMS REST API requests

**Request GET Data Class View**

.. code-block:: console

    GET {BASE_URL}/v1/dataview/{Data Model Name}/{Data View Name}

    where
    {Data Model Name} is the name of the Data Model
    {Data View Name} is the view name of the Data Model


**Description**

Get the Data Class View names associated to {{Data View Name}}


GET Data Class Views associated to the Data View, "Consolidated View"
=====================================================================

Example: To get the Class Views associated to the Data Vieww named, "Consolidated View"

**Request GET**

.. code-block:: console

    {BASE_URL}/v1/dataview/DemoModel/Consolidated View

**Description**

Get the Data Class Views associated to the Data View, "Consolidated View"

**Response**


Response-simplified: showing the Class View Names of the Consolidated View

.. code-block:: console

    Response-simplified: showing the Class View Names of the Consolidated View
    {
    "name": "DemoModel",
    "viewname": "Consolidated View",
    "classes": [
    {
    "name": "ISSUER",
    "viewname": "Issuer",
    "writable": true,
    "attributes": []
    },
    {
    "name": "INSTRUMENT_STK",
    "viewname": "Instrument Stock",
    "writable": true,
    "attributes": []
    },
    {
    "name": "INSTRUMENT_CDS",
    "viewname": "Instrument CDS",
    "writable": true,
    "attributes": []
    },
    {
    "name": "INSTRUMENT_BND",
    "viewname": "Instrument Bond",
    "writable": true,
    "attributes": []
    },
    {
    "name": "LISTING_STK",
    "viewname": "Listing Stock",
    "writable": true,
    "attributes": []
    },
    {
    "name": "LISTING_CDS",
    "viewname": "Listing CDS",
    "writable": true,
    "attributes": []
    },
    {
    "name": "LISTING_BND",
    "viewname": "Listing Bond",
    "writable": true,
    "attributes": []
    },
    {
    "name": "RATING",
    "viewname": "Markit Rating",
    "writable": true,
    "attributes": []
    }
    ]
}

Response-full (JSON): GET Consolidated View Details (Classes & Attributes)

.. code-block:: console

    {
    "name": "C0",
    "viewname": "Consolidated View",
    "classes": [{
            "name": "ISSUER",
            "viewname": "Issuer",
            "writable": true,
            "attributes": [{
                    "name": "C0_IS124",
                    "viewname": "Issuer legal name",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": true,
                    "writable": true
                }, {
                    "name": "C0_SRC25",
                    "viewname": "Issuer entity type",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                },
				(),...,
				{
                    "name": "C0_EOD.NL.JPX.VOLUME",
                    "viewname": "Volume NL.JPX",
                    "type": "TIMESERIES_COMPOUND_INTEGER",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                }
            ]
        }, {
            "name": "INSTRUMENT_STK",
            "viewname": "Instrument Stock",
            "writable": true,
            "attributes": [{
                    "name": "C0_SRC25",
                    "viewname": "Entity type",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                },
				(),...,
				{
                    "name": "C0_INTRADAY.MID",
                    "viewname": "Intraday Mid Price",
                    "type": "TIMESERIES_PRICE",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                }
            ]
        }, {
            "name": "INSTRUMENT_CDS",
            "viewname": "Instrument CDS",
            "writable": true,
            "attributes": [{
                    "name": "C0_SRC25",
                    "viewname": "Entity type",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                },
				(),...,
				{
                    "name": "C0_I0372",
                    "viewname": "Long Description",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": true,
                    "writable": true
                },


            ]
        }, {
            "name": "INSTRUMENT_BND",
            "viewname": "Instrument Bond",
            "writable": true,
            "attributes": [{
                    "name": "C0_EOD.MID",
                    "viewname": "Mid Price",
                    "type": "TIMESERIES_PRICE",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                }
				(),...{}


            ]
        }, {
            "name": "LISTING_STK",
            "viewname": "Listing Stock",
            "writable": true,
            "attributes": [{
                    "name": "C0_SRC25",
                    "viewname": "Entity type",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                },
				(),...,
				{
                    "name": "C0_EOD.C1_TIME",
                    "viewname": "Time",
                    "type": "TIMESERIES_TIME",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                }
            ]
        }, {
            "name": "LISTING_CDS",
            "viewname": "Listing CDS",
            "writable": true,
            "attributes": [{
                    "name": "C0_SRC25",
                    "viewname": "Entity type",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                },
				(),...,
				{
                    "name": "C0_EOD.VOLUME",
                    "viewname": "Volume",
                    "type": "TIMESERIES_INTEGER",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                }
            ]
        }, {
            "name": "LISTING_BND",
            "viewname": "Listing Bond",
            "writable": true,
            "attributes": [{
                    "name": "C0_SRC25",
                    "viewname": "Entity type",
                    "type": "String",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                },
				(),...,
				{
                    "name": "C0_EOD.VOLUME",
                    "viewname": "Volume",
                    "type": "TIMESERIES_INTEGER",
                    "summaryAttribute": false,
                    "descriptionAttribute": false,
                    "writable": true
                }
            ]
        }
    ]

    }











